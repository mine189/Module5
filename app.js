const app = Vue.createApp({
  data() {
    return {
      newInput: '',
      defInput: 'ojHA',
      favMeal: "Pizza"
    }
  },
  methods: {
    setText(event){
      this.newInput=event.target.value;
    },
    saveInput(){
      this.defInput=this.newInput;
    }
  }
});

app.mount('#app');


const app2 = Vue.createApp({
  data() {
    return {
      favMeal: "Pizza"
    };
  }
});

app2.mount('#app2');   